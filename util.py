import numpy as np
import matplotlib.pyplot as plt
import fastfilt as ff

def lt_wav_to_arr(fname):
    ts=[]
    with open(fname) as f:
        w=len(f.readline().split('\t'))-1
        amps=[[] for i in range(w)]
        
        for x in f:
            s=x.split('\t')
            ts.append(float(s[0]))
            for i,v in enumerate(s[1:]):
                amps[i].append(float(v.strip('\r\n')))
    ts=np.asarray(ts)
    ramps=[]
    for arr in amps:
        ramps.append(np.asarray(arr))
    return ts,ramps



def lt_op(fname,steps=500):
    ts,amps=lt_wav_to_arr(fname)

    tmin,tmax=np.min(ts),np.max(ts)
    newts=np.asarray([(tmin+(tmax-tmin)*i/steps) for i in range(steps)])

    ref_amps=[np.interp(newts,ts,amp) for amp in amps]

    return ref_amps,newts
