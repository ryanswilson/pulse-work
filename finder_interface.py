import matplotlib.pyplot as plt
import fastfilt as ff
from scipy import signal
import numpy as np
import sys
import timeit
import re


#mean pulse params
skip_o=20
total_l_o=250


#finder params
f_adjust=200
autolen=800
auto_prev=50

spacer=re.compile(r' *')

class reader():
    def __init__(self,name,block_size=5000,prefilt=None):
        self.source=open(name)
        self.block_size=block_size
        self.eof=False

        self.prefilt=prefilt

    def run_filt(self,vals):
        if not self.prefilt:
            return vals
        else:
            return self.prefilt(vals)

    def get_block(self):
        vals=[]
        for i in xrange(self.block_size):
            t=self.source.readline()
            if t:
                vals.append(int(t)-8192)
            else:
                self.source.close()
                self.eof=True
                return self.run_filt(vals)
        return self.run_filt(vals)

    def close(self):
        self.source.close()
        self.eof=True

class pulse_finder():
    def __init__(self,pulse_name,old_read=True,filt_func=None,**kwargs):

        if old_read:
            skip=kwargs.get('skip',20)
            total_l=kwargs.get('total_l',250)
        else:
            skip = kwargs.get('skip', 3)
            total_l = kwargs.get('total_l', 290)

        if old_read:
            f = open(pulse_name)
            self.mean_pulse = []
            for x in f:
                self.mean_pulse.append(int(x.split('\t')[1][:-1]))
            f.close()

        else:
            f= open(pulse_name)
            self.mean_pulse=[]
            for x in f:
                self.mean_pulse.append(float(x))
            f.close()

        self.mean_pulse = np.asarray(self.mean_pulse[skip:skip + total_l])
        self.mean_pulse=self.mean_pulse-self.mean_pulse[0]

        if old_read:
            pn=kwargs.get('pn',15)
            zn=kwargs.get('zn',15)
        else:
            pn = kwargs.get('pn', 5)
            zn = kwargs.get('zn', 5)

        sForm=kwargs.get('sForm',None)
        #self.acof,self.bcof=ff.prony_shank(self.mean_pulse,pn,zn,unitize=True)
        self.iir_filt=ff.prony_shank(self.mean_pulse,pn,zn,unitize=True,sForm=sForm)

        butter_pole=kwargs.get('butter_pole',3)
        low_freq=kwargs.get('low_freq',.001)

        self.lowb,self.lowa=signal.iirfilter(butter_pole,low_freq,btype='lowpass',ftype='butter')
        if filt_func==None:
            self.filt_func=lambda x:x-signal.filtfilt(self.lowb,self.lowa,x)
        else:
            self.filt_func=filt_func


    def filter_sample(self,sample,**kwargs):
        display=kwargs.get('display',False)

        tsamp=self.filt_func(sample)
        matched=np.flip(self.iir_filt.filter(np.flip(tsamp,0)),0)
        #matched=np.flip(signal.lfilter(self.bcof,self.acof,np.flip(tsamp,0)),0)

        if display:
            plt.plot((tsamp)[f_adjust:])
            #plt.plot(low[f_adjust:])
            plt.plot(matched[f_adjust:])

            plt.title('lowpass, matched filter, and original')
            plt.show()

        else:
            return matched,tsamp

    def schmidt_locater(self,matched,limit=10.0,lower=3.0,ev=False):
        peaks=[]
        if ev:
            evl=[]
        s1=False
        curr_ind=0
        curr_max=lower
        for j in xrange(len(matched)):

            if s1:
                y=matched[j]
                if y>lower:
                    if y>curr_max:
                        curr_max=y
                        curr_ind=j
                else:
                    s1=False
                    peaks.append(curr_ind)
                    if ev:
                        evl.append(matched[curr_ind])
                    curr_max=lower

            else:
                if matched[j]>limit:
                    s1=True
        if ev:
            return peaks,evl
        return peaks

    def find_pulses(self,sample,limit=10.0,lower=3.0,**kwargs):
        block_time=kwargs.get("block_time",None)
        filtered=kwargs.get("filtered",True)
        w_sample=np.asarray(sample)
        outputs=[]

        matched,filt_sig=self.filter_sample(w_sample)

        #the reasonableness of the minimum e functionality independent of
        #the limit on the energy is questionable
        if block_time:
            min_e=kwargs.setdefault("min_e",limit)
            peaks,evs=self.schmidt_locater(matched,limit,lower,ev=True)
            tpeaks=[]
            for i,loc in enumerate(peaks):
                closest=np.min(abs(np.asarray(peaks[0:i]+peaks[i+1:])-loc))
                if closest>block_time:
                    if evs[i]>min_e:
                        tpeaks.append(loc)
            peaks=tpeaks

        else:
            peaks=self.schmidt_locater(matched,limit,lower)

        for loc in peaks:
            if auto_prev+f_adjust<loc<len(w_sample)+autolen:
                if filtered:
                    tp=filt_sig[loc-auto_prev:loc+autolen]
                else:
                    tp=(w_sample)[loc-auto_prev:loc+autolen]
                if len(tp)==auto_prev+autolen:
                    outputs.append(tp)

        return outputs

    def locate_peaks(self,sample,limit=10,lower=3):
        w_sample=np.asarray(sample)

        matched,filtered=self.filter_sample(w_sample)
        peaks=self.schmidt_locater(matched,limit,lower)
        return peaks

    def find_noise(self,sample,window,buffer,**kwargs):
        limit=kwargs.get("limit",10)
        lower=kwargs.get("lower",3)
        mdata_r=kwargs.get("mdata",False)

        peaks=self.locate_peaks(sample,limit,lower)
        peaks.sort()

        rets=[]
        mdata=[]
        prev_ind=peaks[0]
        low=signal.filtfilt(self.lowb,self.lowa,sample)
        for ploc in peaks[1:]:
            if (ploc-prev_ind)>(2*window+2*buffer):
                tsamp=sample[prev_ind+buffer+window:ploc-buffer]
                tsamp=tsamp-low[prev_ind+buffer+window:ploc-buffer]
                rets.append(tsamp)
                if mdata_r:
                    mdata.append(prev_ind,ploc)
            prev_ind=ploc
        if mdata_r:
            return rets,mdata
        return rets

    def integrate_noise(self,reader,window,buffer,N=None,**kwargs):
        limit=kwargs.get("limit",10)
        lower=kwargs.get("lower",3)
        prog=kwargs.get("display",False)
        mode=kwargs.get("mode","fft")

        if mode=="fft":
            nspec=np.zeros(window)
        elif mode=="cc":
            cmat=np.zeros((window,window))
        total=0
        if N==None:
            N=int(1e6)

        dsp_wind=np.blackman(window)

        for i in xrange(N):
            nsamps=self.find_noise(reader.get_block(),window,buffer,limit=limit,lower=lower)
            for samp in nsamps:
                total+=1
                if len(samp)==800:
                    rind=0
                else:
                    rind=np.random.randint(len(samp)-window)
                tsamp=samp[rind:rind+window]
                if mode=="fft":
                    nspec+=np.abs(np.fft.fft(tsamp*dsp_wind))
                elif mode=="cc":
                    cmat+=np.outer(tsamp,tsamp)
            if prog:
                print_prog(i)
            if reader.eof:
                break
        if mode=="fft":
            return nspec/total
        if mode=="cc":
            return cmat/total




    def get_all_pulse(self,file,dest_name,block_size=None,**kwargs):

        prog=kwargs.get('display',False)
        limit=kwargs.setdefault('limit',10.0)
        lower=kwargs.setdefault('lower',3.0)

        if prog:
            c=0

        wf=open(dest_name,'w')
        if block_size:
            file.block_size=block_size
        while not file.eof:
            tp=self.find_pulses(file.get_block(),**kwargs)
            for pulse in tp:
                t=str(np.int16(pulse*100))[1:-1].replace('\n',' ')
                t=' '.join(spacer.split(t))
                if t[0]==' ':
                    wf.write(t[1:]+'\n')
                else:
                    wf.write(t+'\n')
            if prog:
                print_prog(c)
                c+=1
        wf.close()

    def ef(self,sig):
        return np.max(self.iir_filt.filter(np.flip(sig,0)))

def print_prog(n):
    sys.stdout.write(str(n)+'         \r')
    sys.stdout.flush()

def re_average(reader,finder,blocks,**kwargs):
    limit=kwargs.get("limit",10.0)
    lower=kwargs.get("lower",3.0)
    cnt=0
    first=True
    for j in xrange(blocks):
        pulses=finder.find_pulses(reader.get_block(),limit=limit,lower=lower)
        for pulse in pulses:
            if max(pulse)<45:

                if first:
                    acc=pulse
                    first=False
                    cnt+=1
                else:
                    if acc.shape==pulse.shape:
                        acc+=pulse
                        cnt+=1
        print_prog(j)
    return acc/float(cnt)

def write_re_average(name,array):
    f=open(name,'w')
    for item in array:
        f.write(str(item)+'\n')
    f.close()

def time_back(func):
    start_time = timeit.default_timer()
    ret=func()
    elapsed = timeit.default_timer() - start_time
    print (elapsed)
    return ret

def spectrum(fname,e_func):
    es=[]
    with open(fname) as f:
        for event in f:
            v=event.split(' ')
            v=np.asarray(map(lambda x:int(x),v))
            es.append(e_func(v))
    return es

def schmidt_locater(matched,limit=10.0,lower=3.0,ev=False):
    peaks=[]
    if ev:
        evl=[]
    s1=False
    curr_ind=0
    curr_max=lower
    for j in xrange(len(matched)):

        if s1:
            y=matched[j]
            if y>lower:
                if y>curr_max:
                    curr_max=y
                    curr_ind=j
            else:
                s1=False
                peaks.append(curr_ind)
                if ev:
                    evl.append(matched[curr_ind])
                curr_max=lower

        else:
            if matched[j]>limit:
                s1=True
    if ev:
        return peaks,evl
    return peaks

def coef_wavelet(l):
    qb=signal.ricker(200,25)
    filt_base=ff.prony_shank(np.append(qb,np.zeros(400)),14,14,sForm=10)
    s=float(l)/25
    new_wav=signal.ricker(12*l,l)[2*l:]
    filt_base.ac.slow(s)
    filt_base.bc=ff.shank_zeros(new_wav,14,filt_base.ac)
    return filt_base

class wave_spec():
    def __init__(self,detection_l,energy_l,prov_filter=None):
        if prov_filter:
            self.detect_filt=prov_filter
            self.energy_filt=prov_filter
        else:
            self.detect_filt=coef_wavelet(detection_l)
            self.energy_filt=coef_wavelet(energy_l)

        self.ew=detection_l
        self.pad_detect=4*detection_l
        self.pad_energy=4*energy_l

    def find_energies(self,signal,detect_limit=10.0):
        l=len(signal)
        det_wav=self.detect_filt.filter(np.append(signal,np.zeros(self.pad_detect)))[self.pad_detect:]
        e_wav=self.energy_filt.filter(np.append(signal,np.zeros(self.pad_energy)))[self.pad_energy:]
        peaks=schmidt_locater(det_wav,limit=detect_limit,lower=detect_limit-1)

        rspec=[]
        for peak in peaks:
            if self.pad_detect<peak<l-self.pad_detect:
                rspec.append(np.max(e_wav[peak-self.ew:peak+self.ew]))
        return rspec

    def read_spectrum(self,reader,N,limit=10):
        spec=[]
        for i in xrange(N):
            print_prog(i)
            spec+=self.find_energies(reader.get_block(),limit)

            if reader.eof:
                break
        return spec

# example usage, loads from wave3.txt, print number of seconds taken and lists block number
#args=['wave3.txt','-t','-d']
# other param is b which overrides block size

if __name__=='__main__':
    #hard coded arguments
    f_test='smooth_pulse.txt'

    args=sys.argv



    file_list=[]
    display=False
    block_size=5000
    next_is=None
    time=False
    for arg in args[1:]:
        if next_is:
            if next_is=='b':
                block_size=int(arg)

        else:
            next_is=None
            if arg[0]!='-':
                if '.txt' in arg:
                    file_list.append(arg)
            else:
                if arg[1]=='d':
                    display=True
                elif arg[1]=='b':
                    next_is='b'
                elif arg[1]=='t':
                    time=True
                else:
                    raise NameError('Invalid argument %s '% arg)

    finder = pulse_finder(f_test, old_read=False)

    for file in file_list:
        temp_r=reader(file,block_size=block_size)

        write_file=file.split('.')
        write_file=write_file[0]+'_pulses.'+write_file[1]

        func=lambda :finder.get_all_pulse(temp_r,write_file,block_size=block_size,display=display)
        if time:
            time_back(func)
        else:
            func
