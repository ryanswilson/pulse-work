import numpy as np
import tensorflow as tf



def tokenize(s):
    wspc='()\'"'
    for c in wspc:
        s=s.replace(c,' '+c+' ')
    return s.split()

def atom(token):
    try: return int(token)
    except ValueError:
        try: return float(token)
        except ValueError:
            return str(token)
        
def read_tokens(tokens):
    if len(tokens)==0:
        raise NameError('unexpected EOF')
    token=tokens.pop(0)
    if token=='(':
        l=[]
        while tokens[0]!=')':
            l.append(read_tokens(tokens))
        tokens.pop(0)
        return l
    elif token==')':
        raise SyntaxError('unexpected )')
    elif token in '\'"':
        m=token
        s=''
        while tokens[0]!=m:
            s+=str(tokens.pop(0))
        tokens.pop(0)
        return s
    else:
        return atom(token)

def parse(code):
    return read_tokens(tokenize(code))
    
layer_args=['stride','drop']
nonlinear_dict={'relu':tf.nn.relu,'softm':None,'None':lambda x:x}
    
class gen_net():
    def __init__(self,parsed_spec):
        #print parsed_spec
        self.n=0
        
        self.name=parsed_spec[0]
        self.i_shape=parsed_spec[1]
        self.o_shape=parsed_spec[2]
        
        self.X = tf.placeholder(tf.float32, shape=[None]+self.i_shape, name='X')
        self.Y = tf.placeholder(tf.float32, shape=[None]+self.o_shape, name='Y')
        self.kp=tf.placeholder_with_default(np.asarray(0.0,dtype=np.float32),shape=[])
        
        to=self.X
        
        self.train_f=None
        
        with tf.variable_scope(self.name) as scope:
            for lspec in parsed_spec[3]:
                to=self.gen_layer(lspec,to)
                
        self.varl=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES,scope=self.name)
        self.out=to
        self.gen_train(parsed_spec[4],self.out)
        
        self.saver=tf.train.Saver(var_list=varl)
            
    def save(self,sess,fname):
        self.saver.save(sess,fname)
        
    def load(self,fname):
        self.saver.restore(sess,fname)
       
            
    def gen_train(self,spec,inl):
        pass
        #TODO:implement
        #if spec[0]=='cross_entropy_logit':
            #ce=tf.nn.softmax_cross_entropy_with_logit(logits=,labels=)
            #self.train_f=
        
            
    def gen_layer(self,spec,inl):
        arg_dict={}
        for l in spec:
            if type(l)==list:
                if l[0] in layer_args:
                    arg_dict[l[0]]=l[1]    
        
        with tf.variable_scope(str(self.n)):
            skip=False
            if spec[0]=='conv_1d':
                stride=arg_dict.setdefault('stride',1)
                weights=tf.Variable(tf.random_normal( spec[2],stddev=.01 ),dtype=tf.float32)
                bias=tf.Variable(tf.constant(.05,shape=[spec[2][-1]]),dtype=tf.float32)
                
                l=tf.nn.conv1d(value=inl,filters=weights,stride=stride,padding='SAME')
                l=l+bias
            elif spec[0]=='full':
                weights=tf.Variable(tf.random_normal(spec[2],stddev=.01),dtype=tf.float32)
                bias=tf.Variable(tf.constant(.05,shape=[spec[2][-1]]),dtype=tf.float32)
                l=tf.matmul(inl,weights)+bias
            elif spec[0]=='flatten':
                l=tf.reshape(inl,[-1,spec[1]])
                skip=True
                
            else:
                raise NameError('unsupported filter')
            
            if not skip:
                l=nonlinear_dict[spec[1]](l)
                drop=arg_dict.setdefault('drop',False)
                if drop:
                    l=tf.nn.dropout(l,self.kp)
                
        self.n+=1
        print spec
        return l

cmd_dict={'make': gen_net}

def read_nspc(fname):
    with open(fname) as f:
        txt=f.read()
        tree=parse(txt)
        l=[]
        #print tree
        for frag in tree:
            #print frag,frag[0],frag[1]
            l.append(cmd_dict[frag[0]](frag[1]))
    return l
        

