import fastfilt as ff
import matplotlib.pyplot as plt
from scipy import signal
import numpy as np

## --READ THIS--

#I have the average pulse shape saved as ps.txt
#the command to get the filter coefficients is ff.prony_shank(desired shape,number of poles,number of zeros)
#to see the impulse response call signal.lfilter(bcoefs,acoefs,imp)

b=10.0
a=.5

sc=[[-b,1/5.0],[a*b,1/20.0],[(1-a)*b,1/100.0]]

acof,bcof=ff.iir_from_exps(1,sc[0],sc[1],sc[2])
imp=[1]+[0 for i in range(400)]

c=signal.lfilter(bcof,acof,imp)

test=np.asarray([10-abs(i-10) for i in range(21)]+[0 for i in range(15)])
rec=ff.prony_poles(test,12)
#print rec
#plt.plot(signal.lfilter([1],rec,imp[:100]))
#plt.show()

name='ps.txt'
def read_avg_shape(filename):
    f=open(filename)
    vals=[]
    for x in f:
        vals.append(int(x.split('\t')[1][:-1]))

    return vals

avg=np.asarray(read_avg_shape(name)[20:])
avg=(avg-avg[0])[:163]
#avg=(avg-avg[0])[:200]


        


'''pronytest=ff.pronyModel([[-2,1/10.0],[1,1/20.0],[1,1/30.0]],1)

def visualize_n(n,target,model,vg=1):
    for i in xrange(n):
        if i%vg==0: 
            plt.plot(target)
            plt.plot(model.fx(len(target)))
            plt.show()
        model.least_squares_step(target)
        model.fix_value(0,0,target[0])
'''
        
