## Post Bach/Gap Year Work
This is the very academic grade code from my time in Mani Tripathi's lab. This is where I really cut my teeth on python/digital signal processing. 

This work was broadly related to LZ and the development of a neutron scatter camera, and where I learned the important lesson that just because many academic papers use a result doesn't mean the result is actually useful. Specifically there was a figure of merit that is commonly used in the PSD (pulse shape discrimination) literature that I later found was essentially uncorrelated with what we actually cared about, fale positives and negatives.
