import numpy as np
import matplotlib.pyplot as plt
import fastfilt as ff
from scipy import signal
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool
import time

class f_wav():
    def __init__(self,fname):
        self.fname=fname

    def write(self,array):
        f=open(self.fname,'w')
        for x in array:
            f.write(str(x)+'\n')
        f.close()

    def read(self):
        c=[]
        with open(self.fname) as f:
            for l in f:
                c.append(float(l))
        return np.asarray(c)


class pulse():
    def __init__(self,template,Edist,thnoise,photon_response):
        self.template=np.asarray(template).reshape((-1,1))
        self.Edist=Edist
        self.thnoise=thnoise
        self.lshape=photon_response

    def getPulses(self,N,shaping=None):
        es=np.random.choice(self.Edist,(1,N))
        work=np.dot(self.template,es)
        work=np.asarray(np.random.poisson(work),dtype=np.float32)

        if self.lshape:
            work=np.apply_along_axis(lambda x:self.lshape.filter(x),0,work)
        if shaping:
            work = np.apply_along_axis(lambda x: shaping.filter(x), 0, work)
        work+=self.thnoise*np.random.normal(np.zeros(work.shape))
        return work

class dec_pulse():
    def __init__(self,ts,portions,Edist,thnoise,photon_response,tres,steps):
        self.ts=ts
        self.portions=portions
        self.Edist=Edist
        self.thnoise=thnoise
        self.lshape=photon_response
        self.tr=tres
        self.steps=steps

    def getPulses(self,N,shaping=None):
        phots=self.photons(N)
        bins=np.linspace(0,self.tr*self.steps,self.steps+1)
        rets=[]
        for i in xrange(N):
            pd=np.histogram(phots[i],bins=bins)
            work=self.lshape.filter(np.asarray(pd[0]))
            if self.thnoise:
                work+=np.random.normal(np.zeros(self.steps))*self.thnoise
            rets.append(work)
        return rets

    def photons(self,N):
        es=np.random.choice(self.Edist,(N))
        phots=[]
        for i in xrange(N):
            e=es[i]
            nums=[]
            erem=e
            for j in xrange(len(self.portions)-1):
                nums.append(np.random.binomial(erem,self.portions[j]))
                erem-=nums[j]
            nums.append(erem)
            tw=[[self.ts[j]]*nums[j] for j in xrange(len(self.ts))]
            ts=np.asarray(reduce(lambda x,y:x+y,[[]]+tw))
            ts=-np.log(np.random.uniform(np.zeros(e)))*ts
            phots.append(ts)
        return phots



def exp_former(x,*args):
    acc=np.zeros(x.shape)
    for j in xrange(0,len(args)-1,2):
        acc+=args[j]*np.exp(-x*args[j+1])
    return acc

def save_moments(part,N,name_stem):
    vals=part.getPulses(N)
    uvals=vals/np.sum(vals,0).reshape((1,-1))
    m1=np.sum(uvals,1)/N
    m2=np.sum(uvals**2,1)/N

    f_wav(name_stem+'m1.txt').write(m1)
    f_wav(name_stem+'m2.txt').write(m2)

def clip(vs,p=.1):
    ul=np.percentile(vs,100.0-p)
    ll=np.percentile(vs,p)
    return np.clip(vs,ll,ul)

def get_vals(p1,p2,N,wt):
    wt=np.reshape(wt,(-1,1))
    set1=p1.getPulses(N)
    set2=p2.getPulses(N)
    ps = np.append(set1, set2, 1)
    psx = np.sum(ps, 0)
    psy = np.sum(ps * wt, 0)
    vals = psy / psx

    return vals

def cut_amp(pulses,cut_amp):
    return pulses[:,np.sum(pulses,0)>cut_amp]

def hist_up(vals,hist=None,hist_x=None,bins=1000):
    #is sum on axis 1 or 0 or why is this not working, needs serious look
    #wt=np.reshape(wt_a,(-1,1))

    #ps=np.append(set1,set2,1)
    #psx=np.sum(ps,0)
    #psy=np.sum(ps*wt,0)
    #vals=psy/psx

    vals=clip(vals)

    if hist is None:
        hist,hist_x=np.histogram(vals,bins=bins)
        hist[0]=0
        hist[-1]=0
    #probably broken for multiple runs
    else:
        hist_t,hist_x=np.histogram(vals,bins=hist_x)
        hist_t[0]=hist_t[-1]=0
        hist+=hist_t

    return hist,hist_x

def half_peak_bound(m_ind,binvals):

    w=m=binvals[m_ind]
    w_ind=m_ind
    while w>m/2.0:
        w_ind+=1
        w=binvals[w_ind]

    pu=w_ind-(m/2.0-w)/(binvals[w_ind-1]-w)

    w = m = binvals[m_ind]
    w_ind = m_ind
    while w > m / 2.0:
        w_ind+=-1
        w = binvals[w_ind]

    pl=w_ind+(m/2.0-w)/(binvals[w_ind+1]-w)

    return pl,pu


def FOM(binvals,lp=False,lpw=4):
    try:
        if lp:
            bc,ac=signal.iirfilter(4,1/float(lpw),btype='lowpass')
            binvals=signal.filtfilt(bc,ac,binvals)

        m_ind=0
        m_val=0
        for i in xrange(len(binvals)):
            if binvals[i]>m_val:
                m_val=binvals[i]
                m_ind=i

        pl1,pu1=half_peak_bound(m_ind,binvals)
        m_ind1=m_ind

        m_ind=0
        m_val=0
        for i in xrange(len(binvals)):
            if (binvals[i]>m_val) and not pl1<i<pu1:
                m_val=binvals[i]
                m_ind=i

        pl2, pu2 = half_peak_bound(m_ind, binvals)
        return abs(m_ind-m_ind1)/(pu1-pl1+pu2-pl2)
    except:
        return 0

def eval_l(l,set1,set2=None,lpw=None,bins=1000,amp_cut=None):
    lpt = bool(lpw)
    lm=set1.shape[0]

    if set2==None:
        set2=np.zeros([lm,0])

    wt = np.asarray([0] * (l) + [1] * (lm - l)).reshape((-1, 1))
    ps = np.append(set1, set2, axis=1)

    if amp_cut:
        ps=ps[:,np.sum(ps,0)>amp_cut]

    vals = np.sum(wt * ps, 0) / np.sum(ps, 0)
    vals=clip(vals)
    hist,hist_x=np.histogram(vals,bins=1000)
    return FOM(hist, lpt, lpw)

def tail_start_monte_search(p1,p2,N,lpw=None):
    f_dict={}

    set1=p1.getPulses(N)
    set2=p2.getPulses(N)

    lm=set1.shape[0]

    #low passing binvals is used to get more stable results
    #calculate FOM using block wt


    checked_l=[]

    for i in range(int(lm*.05),lm,int(lm*.05)):
        checked_l.append(i)
        f_dict[i]=eval_l(i,set1,set2,lpw)

    checked_l.sort(key=lambda x:f_dict[i],reverse=True)

    s=[checked_l[0],checked_l[1]]
    for i in range(min(s),max(s),5):
        if i not in checked_l:
            checked_l.append(i)
            f_dict[i] = eval_l(i,set1,set2,lpw)

    checked_l.sort(key=lambda x: f_dict[i], reverse=True)

    return f_dict[checked_l[0]],f_dict

def hist_from_precomp(set1,set2,wt,bins=1000):
    wt=np.reshape(wt,[-1,1])
    ps=np.append(set1,set2,1)
    psx = np.sum(ps, 0)
    psy = np.sum(ps * wt, 0)
    vals = psy / psx
    h,x=hist_up(vals,bins=bins)
    return h,FOM(h,True,10),x

def FOM_handle(p1,p2,N,wt,runs=1,**kwargs):

    threads=kwargs.get('threads',False)
    time_code=kwargs.get('time',False)
    progress=kwargs.get('progress',False)

    if time_code:
        t0=time.time()

    if threads:
        pool=ThreadPool(threads)

        val_list=pool.map(lambda x:get_vals(p1,p2,N,wt),range(runs))

        pool.close()
        pool.join()

        hist,hist_x=None,None
        for vl in val_list:
            hist,hist_x=hist_up(vl,hist,hist_x)

    else:

        hist,hist_x=hist_up(get_vals(p1,p2,N,wt))

        for i in xrange(runs-1):
            if progress:
                print i
            hist,hist_x=hist_up(get_vals(p1,p2,N,wt),hist=hist,hist_x=hist_x)

    if time_code:
        t1=time.time()
        print t1-t0

    hist_r=kwargs.get('hist',False)
    if hist_r:
        return hist,hist_x
    else:
        return FOM(hist)

def gatti_snr(wt,p1m1,p1m2,p2m1,p2m2):
    wt=np.asarray(wt)
    p1m1=np.asarray(p1m1)
    p1m2=np.asarray(p1m2)
    p2m1=np.asarray(p2m1)
    p2m2=np.asarray(p2m2)

    delt=np.sum(wt*(p1m1-p2m1))
    eps2=np.sum(wt**2*(p1m2+p2m2))

    return delt**2/eps2

#ntemp=exp_former(np.arange(1000), .0872, .465, .0734  , .0575, .054  , .00602)
#ntemp=ntemp/np.sum(ntemp)
#gtemp=exp_former(np.arange(1000), .0872, .465, .0734/2, .0575, .054/5, .00602)
#gtemp=gtemp/np.sum(gtemp)

#Emono=[500 for i in xrange(100)]

#sipm_dark=f_wav('smooth_pulse.txt').read()
#sipm_dark=sipm_dark[3:]-sipm_dark[-1]

#sfilt=ff.prony_shank(sipm_dark,4,4,sForm=10)

#pgamma=pulse(gtemp,Emono,0,None)
#pneutron=pulse(ntemp,Emono,0,None)

#pgamma=pulse(gtemp,Emono,60,sfilt)
#pneutron=pulse(ntemp,Emono,60,sfilt)

#imp=[1]+[0]*999

#pgm1=f_wav('pgm1.txt').read()
#pgm2=f_wav('pgm2.txt').read()
#pnm1=f_wav('pnm1.txt').read()
#pnm2=f_wav('pnm2.txt').read()

#varp=pnm2-pnm1**2
#varg=pgm2-pgm1**2

#gatti_num=pnm1-pgm1
#gatti_denom=varp+varg
#gatti_wt=gatti_num/gatti_denom

#t2gm1=f_wav('testmm1.txt').read()
#t2gm2=f_wav('testmm2.txt').read()
#t2nm1=f_wav('testnm1.txt').read()
#t2nm2=f_wav('testnm2.txt').read()

#tvn=t2nm2-t2nm1**2
#tvg=t2gm2-t2gm1**2

#t_num=t2nm1-t2gm1
#t_denom=tvn+tvg
#t_wt=t_num/t_denom
