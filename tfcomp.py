import numpy as np
import tensorflow as tf
import pulse_gen as pg
import matplotlib.pyplot as plt

accdata=True
if accdata:
    import sys
    accdir="../data/acq_2017_AmBE/"
    sys.path.append("../data/acq_2017_AmBE")
    import acc
    sand=acc.get_pulses(accdir)



sess=tf.InteractiveSession()

eps=10**-8

batch_size=100000-1

#to toggle training of integration window in the err section remove self.tb
#from the var list of self.trn
class filt_opt():
    def __init__(self,m1_d,m2_a,N,ti,s=10):
        self.ac=m2_a
        self.dc=m1_d
        self.s=s

        self.amps=tf.Variable(tf.random_normal((N,1)),dtype=tf.float32)
        self.ts=tf.Variable(tf.random_normal((N,1),mean=10,stddev=1.5),dtype=tf.float32)
        self.tb=tf.Variable(np.asarray(ti).reshape((2,1)),dtype=tf.float32)
        self.thresh=tf.Variable(np.asarray([0]),dtype=tf.float32)
        self.w=tf.placeholder_with_default(np.asarray(20,dtype=np.float32),shape=[])

        self.N=N
        self.l=len(self.ac[0:-1:self.s])
        self.xs=tf.range(0,self.l,dtype=tf.float32)
        self.block=tf.sigmoid((self.xs-self.tb[0,0])*self.w)-tf.sigmoid((self.xs-self.tb[1,0])*self.w)

        self.err_v=None
        self.trn_v=None

        self.saver=tf.train.Saver(var_list=[self.amps,self.tb,self.ts,self.thresh])

        self.wt = tf.reduce_sum(
            tf.exp(-tf.reshape(self.xs, (self.l, 1)) / tf.reshape(self.ts, (1, self.N))) * tf.reshape(self.amps,
                                                                                                      (1, self.N)),
            axis=1)

        cross_tensor = i_cube(self.l)
        shift_w = tf.transpose(tf.tensordot(self.wt, cross_tensor, [[0], [0]]))

        self.eff_wt = tf.tensordot(shift_w, self.block, [[0], [0]])


    def err_from_pulse(self):
        self.part1=tf.placeholder(tf.float32,shape=[batch_size,self.l])
        self.part2=tf.placeholder(tf.float32,shape=[batch_size,self.l])

        p1amps=tf.reduce_sum(self.part1,axis=1)
        p2amps=tf.reduce_sum(self.part2,axis=1)

        p1filt=tf.tensordot(self.part1,self.eff_wt,[[1],[0]])
        p2filt =tf.tensordot(self.part2, self.eff_wt, [[1], [0]])

        p1outs=p1filt/p1amps-self.thresh
        p2outs =p2filt / p2amps - self.thresh

        #for actual logits, can remove sigmoid, but for L2 needs sigmoid
        self.logits=tf.nn.sigmoid(tf.concat([p1outs,p2outs],axis=0))

        self.labels=tf.concat([tf.ones(p1outs.shape,dtype=tf.float32),tf.zeros(p2outs.shape,dtype=tf.float32)],axis=0)
        #self.pulse_cost=tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=labels,logits=logits))
        self.pulse_cost=tf.reduce_sum((self.logits-self.labels)**2)
        self.pulse_trn=tf.train.AdamOptimizer().minimize(self.pulse_cost,var_list=[self.amps,self.ts,self.tb,self.thresh])
        return self.pulse_trn

    #p1dat p2dat?=sim pulse library
    def train_pulse_err(self,steps,p1_dat,p2_dat,display=None,l=5000.0,g=1.0):
        p1_dat=np.transpose(p1_dat)
        p2_dat=np.transpose(p2_dat)
        p1m=p1_dat.shape[0]
        p2m=p2_dat.shape[0]
        for i in xrange(steps):
            i1=np.random.randint(0,p1m-batch_size)
            i2 = np.random.randint(0, p2m - batch_size)

            feed_dict={}
            feed_dict[self.part1]=p1_dat[i1:i1+batch_size,0:-1:self.s]
            feed_dict[self.part2]=p2_dat[i2:i2+batch_size,0:-1:self.s]
            feed_dict[self.w]= g+i/l

            sess.run(self.pulse_trn,feed_dict=feed_dict)

            if display and i%display==0:
                self.plot(bw=g+i/l)
                print sess.run(self.pulse_cost,feed_dict=feed_dict)
                plt.show()

    def err(self):

        delt=tf.reduce_sum(self.eff_wt*self.dc[0:-1:self.s])
        eps_2=tf.reduce_sum(self.ac[0:-1:self.s]*self.eff_wt**2)
        self.snr_v=eps_2/delt**2

        self.trn_v=tf.train.AdamOptimizer().minimize(self.snr_v,var_list=[self.amps,self.ts,self.tb])
        return self.trn_v



    def train_err(self,steps,display=2000):
        for i in xrange(steps):
            sess.run(self.trn_v,feed_dict={self.w:.1+i/10000.0})

            if display and i%display==0:
                self.plot(bw=.1+i/10000.0)
                print self.snr()
                plt.show()

    def save(self,fname):
        self.saver.save(sess,fname)

    def restore(self,fname):
        self.saver.restore(sess,fname)

    def plot(self,block=True,eff_wt=True,opt=True,board=True,bw=1.0):
        if board:
            t=sess.run(self.wt)
            v=max(np.max(t),-np.min(t))
            plt.plot(t/v)
        if eff_wt:
            t=sess.run(self.eff_wt,feed_dict={self.w:bw})
            v=max(np.max(t),-np.min(t))
            plt.plot(t/v)
        if block:
            t=sess.run(self.block,feed_dict={self.w:bw})
            plt.plot(t/np.max(t)*.5)
        if opt:
            t= ((self.dc+eps)/(self.ac+eps))[0:-1:self.s]
            plt.plot(t/np.max(t))

    def snr(self,wt=None):
        if wt is None:
            wt=sess.run(self.eff_wt)
            l=len(wt)
            wt=np.interp(range(self.s*l),range(0,self.s*l,self.s),wt)
            wt=wt.reshape((-1,1))
        else:
            wt=np.asarray(wt).reshape((-1,1))
        eps=np.sum(wt**2*self.ac.reshape((-1,1)))
        delt=np.sum(wt*self.dc.reshape((-1,1)))
        return eps/delt**2
        


def i_cube(N):
    mat=np.zeros((N,N,N),dtype=np.float32)

    for i in xrange(N):
        for j in xrange(N-i):
            mat[i,j,j+i]=1

    return mat


# Simulation Code

tfilt=filt_opt(pg.gatti_num,pg.gatti_denom,6,(5,100))
trn=tfilt.err_from_pulse()

tf.global_variables_initializer().run()

#remove thresh from variable list for test 4
#tfilt.restore('paras/test4')

#temp test
#tentative window at 50??
#vs=pool.map(lambda l:(l,pg.eval_l(l,set1,set2,8.0),tfilt.snr([0]*l+[1]*(1000-l))),range(1000))
pg_test=False
if pg_test:
    dt=sess.run(tfilt.eff_wt)
    dt=np.interp(range(1000),range(0,1000,10),dt)
    dt=np.reshape(dt,(-1,1))
    da,db=pg.hist_up(pg.get_vals(pg.pgamma,pg.pneutron,2000,dt))

batch_test=False
if batch_test:
    neudata=pg.pneutron.getPulses(100000)
    gamdata=pg.pgamma.getPulses(100000)

