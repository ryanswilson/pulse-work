import numpy as np
import math
import numpy.polynomial as poly
from scipy import signal


class rationalPoly():
    def __init__(self,num,denom):
        self.numerator=num
        self.denominator=denom

    def __add__(self, other):
        denom=self.denominator*other.denominator
        num=self.numerator*other.denominator+self.denominator*other.numerator
        return rationalPoly(num,denom)

    def __sub__(self, other):
        denom=self.denominator*other.denominator
        num=self.numerator*other.denominator-self.denominator*other.numerator
        return rationalPoly(num,denom)

    def __mul__(self, other):
        num=self.numerator*other.numerator
        denom=self.denominator*other.denominator
        return rationalPoly(num,denom)

    def __div__(self, other):
        num=self.numerator*other.denominator
        denom=self.denominator*other.numerator
        return rationalPoly(num,denom)

#exp is in z domain as a/(z-b)
class exp():
    def __init__(self,amp,tdecay,timestep):
        self.a=amp
        self.td=tdecay
        self.ts=timestep
        self.b=math.e**(-tdecay*timestep)

    def td_up(self,new_td):
        self.td=new_td
        self.b=b=math.e**(-self.td*self.ts)


    def z(self,**kwargs):
        bilin=kwargs.get('bilin',False)


        return rationalPoly(poly.Polynomial([self.a]),poly.Polynomial([1,-self.b]))

    def fx(self,len):
        outs=[self.a]
        for i in range(len-1):
            outs.append(outs[-1]*self.b)

        return np.asarray(outs)

class poly_chunk():
    def __init__(self,polys):
        self.chunks=polys
        self.l=0

    def add_chunk(self,chunk):
        self.chunks.append(chunk)
        self.l+=len(chunk)

    def filter(self,targ,mode='a'):

        w=targ
        for poly in self.chunks:
            if mode=='a':
                w = signal.lfilter([1],poly,w)
            elif mode=='b':
                w=signal.lfilter(poly,[1],w)
            else:
                raise NameError('unsupported mode for poly chunk')
        return w

    def roots(self):
        rts=np.zeros((0))
        for chnk in self.chunks:
            rts=np.append(poly.Polynomial(np.flipud(chnk)).roots(),rts)
        return rts

    def slow(self,s):
        new_chunks=[]
        for chnk in self.chunks:
            wk=slow_roots(chnk,s)
            new_chunks+=wk.chunks
        self.chunks=new_chunks

class chunk_iir():
    def __init__(self,a_chunk,b_chunk):
        self.ac=a_chunk
        self.bc=b_chunk

    def filter(self,targ):
        return self.bc.filter(self.ac.filter(targ,mode='a'),mode='b')

    def fit_err(self,target):
        l=len(target)
        imp=np.asarray([1]+[0]*(l-1))
        out=self.filter(imp)
        return np.sum((out-target)**2)

    def analog_vals(self,T):
        ar=self.ac.roots()
        br=self.bc.roots()

        #ar yeilds poles, br yeilds zeros
        return np.log(ar)/T,np.log(br)/T


#takes in 1/z poly starting with 1
def slow_roots(coefs,s):
    roots=poly.Polynomial(np.flipud(coefs)).roots()
    roots.sort()

    poly_frags=[]
    work_roots=[]

    for rt in roots:
        if len(work_roots)==0:
            work_roots.append(rt)

        elif np.abs(np.conjugate(work_roots[-1])-rt)<10**-8:
            trts=[np.exp(np.log(work_roots.pop(-1))/s),np.exp(np.log(rt)/s)]

            tp=np.real(np.flipud(poly.Polynomial.fromroots(trts).coef))
            poly_frags.append(tp)

        else:
            work_roots.append(rt)

    work_roots=np.exp(np.log(np.asarray(work_roots))/s)
    if len(work_roots)>0:
        tp=np.real(np.flipud(poly.Polynomial.fromroots(work_roots).coef))
        poly_frags.append(tp)

    return poly_chunk(poly_frags)


def prony_poles(target,M):
    N=len(target)
    b=np.asarray(target[M:]).reshape((-1,1))

    A=np.zeros((N-M,0))
    for x in xrange(M):
        A=np.append(target[x:x+N-M].reshape((-1,1)),A,axis=1)

    ps=np.dot(np.linalg.pinv(A),b)

    #this return is of the form usable by scipy.signal.lfilter
    return np.append(1,-ps)

def shank_zeros(target,zn,afilt,unit_len=None):
    zn=zn+1
    #this is necessary to get zn+1 coef, which will give properly zn zeros

    l=len(target)
    imp=np.asarray([1]+[0]*(l-1))
    g=afilt.filter(imp,mode='a').reshape((-1,1))
    g_mat = np.zeros((l, 0))

    for j in xrange(zn):
        g_mat = np.append(g_mat, g, axis=1)
        g = np.roll(g, 1)
        g[0] = 0

    bcof = np.dot(np.linalg.pinv(g_mat), np.reshape(target, (-1, 1))).reshape((-1,))

    if unit_len:
        amp=np.sum(signal.lfilter(bcof,[1],afilt.filter([1]+[0]*(unit_len-1),mode='a')))
        bcof/=amp

    bfilt=poly_chunk([bcof])

    return bfilt

def prony_shank(target_a,pn,zn,unitize=False,unit_len=500,sForm=None):
    l=len(target_a)
    target=np.asarray(target_a)
    if sForm:
        acof= prony_poles(target[0:-1:sForm],pn)
        afilt=slow_roots(acof,sForm)
    else:
        acof = prony_poles(target, pn)
        afilt=poly_chunk([acof])

    bfilt=shank_zeros(target,zn,afilt,unit_len if unitize else None)

    return chunk_iir(afilt,bfilt)




#takes comma seperated args where each is [amplitude,decay time]
def iir_from_exps(timestep,*args):

    polys=[]

    for arg in args:

        polys.append(exp(arg[0],arg[1],timestep).z())

    retpoly=polys[0]
    for tpoly in polys[1:]:
        retpoly+=tpoly

    a=retpoly.denominator.coef
    b=retpoly.numerator.coef

    return a,b
